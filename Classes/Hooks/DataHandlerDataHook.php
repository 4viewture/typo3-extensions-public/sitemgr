<?php

namespace KayStrobach\Sitemgr\Hooks;


use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use KayStrobach\Sitemgr\Domain\Service\CustomerService;
use KayStrobach\Sitemgr\Utilities\FileSystemUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DataHandlerDataHook
{
    const GROUP_PREFIX = 'C: ';

    /**
     * @param string $status (new or edit)
     * @param string $table
     * @param int $id
     * @param array $fieldArray
     * @param DataHandler $pObj
     */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, $fieldArray, $pObj)
    {
        // @todo check wether mainBeuser exists
        // @todo check wether users have correct groups
        // ...
        if ($table === 'pages') {
            if ($status === 'new') {
                if (isset($pObj->substNEWwithIDs[$id])) {
                    if (isset($fieldArray['doktype']) && ((int)$fieldArray['doktype'] === CustomerRepository::CUSTOMER_DOCTYPE)) {
                        $this->handleNewEntry($table, $pObj->substNEWwithIDs[$id], $fieldArray, $pObj);
                    }
                }
            } else {
                // @todo verify if a sitemgr has been edited. normally this should not be needed
                //$this->handleEditEntry($table, $id, $fieldArray, $pObj);
            }
        }
    }

    public function handleNewEntry($table, $id, $fieldArray, $pObj)
    {
        // @todo fetch subgroups from pageTS
        $defaultGroup = $GLOBALS["BE_USER"]->getTSConfig(
            'mod.web_txsitemgr.customer.createUser.group',
            \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($id)
        );

        $data = [
            'be_groups' => array(
                'NEW41' => array(
                    'pid' => 0,
                    'title' => self::GROUP_PREFIX . $fieldArray['title'] . ' (pid=' . (int)$id . ')',
                    'hidden' => 0,
                    'subgroup' => $defaultGroup['value'],
                    'db_mountpoints' => $id,
                ),
            ),
            //create user
            'be_users' => array(
                'NEW31' => array(
                    'pid' => 0,
                    'username' => $fieldArray['title'],
                    'realName' => $fieldArray['title'] . '-administrator',
                    'email' => '',
                    'password' => '',
                    'usergroup' => 'NEW41',
                    'fileoper_perms' => 15,
                    'lang' => $GLOBALS['BE_USER']->uc['lang'],  // set this user lang as default language for the new user
                    'options' => 2,
                    'db_mountpoints' => $id,
                ),
            ),
            'pages' => [
                $id => [
                    'main_be_user' => 'NEW31',
                    'be_groups' => 'NEW41',
                    'hidden' => 0,
                ]
            ],
        ];
        $tcemain = $this->processDatabaseOperations($data);
        $groupUid = $tcemain->substNEWwithIDs['NEW41'];
        $userUid = $tcemain->substNEWwithIDs['NEW31'];

        $data = [
            'tx_beacl_acl' => [
                'NEW51' => [
                    'pid' => $id,
                    'type' => 0,
                    'object_id' => $userUid,
                    'cruser_id' => $userUid,   //set creator to owner
                    'permissions' => 27,       //do not delete rootpage, but allow all other things
                    'recursive' => 0,
                ],
                'NEW52' => [
                    'pid' => $id,
                    'type' => 0,
                    'object_id' => $userUid,   //allow all for subpages
                    'cruser_id' => $userUid,   //set creator to owner
                    'permissions' => 31,
                    'recursive' => 1,
                ],
            ],
            'sys_template' => [
                'NEW66' => [
                    'pid' => $id,
                    'title' => 'Customer Template'
                ]
            ]
        ];
        $this->processDatabaseOperations($data);

        /** @var CustomerService $customerService */
        $customerService = GeneralUtility::makeInstance(CustomerService::class);
        $customerService->ensureUserHomeFolderExists($userUid);
        $customerService->ensureGroupHomeFolderExists($groupUid);
        BackendUtility::setUpdateSignal('SiteMgr::startCustomerEdit', ['id' => $id]);
    }

    protected function handleEditEntry($table, $id, $fieldArray, $pObj)
    {
        if (!isset($fieldArray['title'])) {
            return;
        }

        $pageEntry = BackendUtility::getRecord(
            'pages',
            $id,
            'title,main_be_user,admin_be_users,normal_be_users,be_groups'
        );

        #if ($fieldArray['title'] === $pageEntry['title']) {
        #    return;
        #}

        $data = [];

        foreach (explode(',', $pageEntry['be_groups']) as $group) {
            $data['be_groups'][$group]['title'] = self::GROUP_PREFIX . $fieldArray['title'] . ' (pid=' . (int)$id . ')';
        }

        if ((isset($pageEntry['main_be_user'])) && (strlen($pageEntry['main_be_user']) > 0)) {
            $data['be_users'][$pageEntry['main_be_user']]['username'] = $fieldArray['title'];
        }
        $this->processDatabaseOperations($data);
        BackendUtility::setUpdateSignal('SiteMgr::startCustomerEdit', ['id' => $id]);
    }

    /**
     * @param $data
     * @return \TYPO3\CMS\Core\DataHandling\DataHandler
     */
    protected function processDatabaseOperations($data)
    {
        /** @var \TYPO3\CMS\Core\DataHandling\DataHandler $tcemain */
        $tcemain = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
        $tcemain->start(
            $data,
            array(),
            $this->getBackendUserAsAdmin()
        );
        $tcemain->process_datamap();
        return $tcemain;
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUserAsAdmin()
    {
        /** @var BackendUserAuthentication $user */
        $user = clone $GLOBALS['BE_USER'];
        $user->user['admin'] = 1;
        return $user;
    }
}