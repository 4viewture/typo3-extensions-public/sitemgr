﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _start:

=============================================================
Sitemanager and Customer Management
=============================================================

.. only:: html

	:Classification:
		sitemgr

	:Version:
		|release|

	:Language:
		en

	:Description:
		The sitemgr allows you to manage a number of customers in one TYPO3 installation and delegate usermanagement to
    		the customer administrator.

	:Keywords:
		customer, sitemgr, backend, user

	:Copyright:
		2017

	:Author:
		Kay Strobach

	:Email:
		typo3@kay-strobach.de

	:License:
		This document is published under the Open Publication License
		available from http://www.opencontent.org/openpub/

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <https://typo3.org/>`__.


	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	UsersManual/Index
	AdministratorManual/Index
	DataStructures/Index
	Sponsors
	Links
