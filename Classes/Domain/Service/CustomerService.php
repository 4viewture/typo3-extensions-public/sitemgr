<?php

namespace KayStrobach\Sitemgr\Domain\Service;


use KayStrobach\Sitemgr\Domain\Model\Customer;
use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use KayStrobach\Sitemgr\Domain\Service\Exception\InvalidFormDataException;
use KayStrobach\Sitemgr\Domain\Service\Exception\SettingMissingException;
use KayStrobach\Sitemgr\Utilities\FormEngineUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Resource\Exception\ExistingTargetFolderException;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class CustomerService implements SingletonInterface
{
    /**
     * @var \KayStrobach\Sitemgr\Utilities\FormEngineUtility
     */
    protected $formEngineUtility;

    public function __construct()
    {
        $this->formEngineUtility = GeneralUtility::makeInstance(FormEngineUtility::class);
    }

    public function isUserAdministratorOfCustomer(Customer $customer, BackendUserAuthentication $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
        if ($user->check('custom_options', 'tx_sitemgr_custom:permissions')) {
            return true;
        }

        if (($customer->getMainBeUser() !== null) && ($customer->getMainBeUser()->getUid() === $user->user['uid'])) {
            return true;
        }
        /** @var \TYPO3\CMS\Extbase\Domain\Model\BackendUser $customerUser */
        if ($customer->getAdminBeUsers() instanceof \Iterator) {
            foreach ($customer->getAdminBeUsers() as $customerUser) {
                if ($customerUser->getUid() === $user->user['uid']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function isUserOfCustomer(Customer $customer, BackendUserAuthentication $user)
    {
        if ($this->isUserAdministrator($customer, $user)) {
            return true;
        }
        /** @var \TYPO3\CMS\Extbase\Domain\Model\BackendUser $customerUser */
        foreach ($customer->getNormalBeUsers() as $customerUser) {
            if ($customerUser->getUid() === $user->user['uid']) {
                return true;
            }
        }
        return false;
    }

    public function createBackendUserForCustomer(Customer $customer, $arg)
    {
        $extConfig = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['sitemgr']);
        if ($extConfig['forceBeUserPrefix']) {
            if(substr($arg['username'],0,strlen($customer->getTitle()))!==$customer->getTitle()) {
                $arg['username'] = $customer->getTitle() . '-' . $arg['username'];
            }
        }

        $arg['pid'] = 0;
        $arg['usergroup'] = $customer->getBeGroupsCommaSeparated();

        $tce = $this->formEngineUtility->handleData(
            [
                'be_users' => [
                    'NEW39459' => $arg
                ]
            ],
            [],
            [],
            true
        );

        $beUserUid = $tce->substNEWwithIDs['NEW39459'];

        $erg = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
            'pages',
            'uid=' . $customer->getUid(),
            [
                'normal_be_users' => $customer->getNormalBeUsersCommaSeparated() . ',' . $beUserUid
            ]
        );
    }

    public function createCustomer($arg)
    {
        /***********************************************************************
         * fetch needed options
         */
        $tgroup = $GLOBALS["BE_USER"]->getTSConfig(
            'mod.web_txsitemgr.customer.createUser.group',
            \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($arg['pid'])
        );
        if (!strlen(trim($tgroup['value']))) {
            throw new SettingMissingException(
                'Sry, but you need to define mod.web_txsitemgr.customer.createUser.group in PageTS to ensure proper user rights' . print_r($arg, true),
                1491762898
            );
        }

        /***********************************************************************
         * create first step records
         */
        //------------------------------------------------------------------
        //be_groups & be_users
        $data = array(
            //create page
            'pages' => array(
                'NEW11' => array(
                    'pid' => $arg['pid'],
                    'doktype' => 4,
                    'title' => $arg['title'],
                    'nav_title' => $arg['title'],
                    'description' => $arg['description'],
                    'hidden' => 0,
                    'shortcut_mode' => 1,
                    'alias' => $arg['title'],
                    'editlock' => 1,
                ),
                //create dummy page
                'NEW13' => array(
                    'pid' => 'NEW11',
                    'hidden' => '0',
                    'title' => 'Start'
                ),
            ),
            //create group
            'be_groups' => array(
                'NEW41' => array(
                    'pid' => 0,
                    'title' => 'E: ' . $arg['title'],
                    'hidden' => 0,
                    'subgroup' => $tgroup['value'],
                    'db_mountpoints' => 'NEW11',
                ),
            ),
            //create user
            'be_users' => array(
                'NEW31' => array(
                    'pid' => 0,
                    'username' => $arg['title'],
                    'realName' => $arg['title'] . '-admin',
                    'email' => $arg['customerEmail'],
                    'password' => $arg['password'],
                    'usergroup' => 'NEW41',
                    'fileoper_perms' => 15,
                    'lang' => $GLOBALS['BE_USER']->uc['lang'],  // set this user lang as default language for the new user
                    'options' => 2,
                    'db_mountpoints' => 'NEW11',
                ),
            ),
        );

        //execute hook
        $this->executeDatabasePreprocessing(
            'customerCreateRound2',
            $data,
            array(
                'parentUid' => $arg['uid'],
                'description' => $arg['description'],
                'customerName' => $arg['title'],
                'customerParentGroup' => $tgroup['value'],
                'customerEmail' => $arg['customerEmail'],
                'customerPassword' => $arg['password'],
                'defaultLang' => $GLOBALS['BE_USER']->uc['lang'],
            )
        );


        /** @var \TYPO3\CMS\Core\DataHandling\DataHandler $tcemain */
        $tcemain = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
        $tcemain->start(
            $data,
            array(),
            $this->getBackendUserAsAdmin()
        );
        $tcemain->process_datamap();
        $groupId = $tcemain->substNEWwithIDs['NEW41'];
        $userId = $tcemain->substNEWwithIDs['NEW31'];
        $pageId = $tcemain->substNEWwithIDs['NEW11'];

        /***********************************************************************
         * create second step records
         */
        $data = array(
            //create template
            'sys_template' => array(
                'NEW21' => array(
                    'pid' => $pageId,
                    'constants' => '######################################################################' . PHP_EOL .
                        '# EXT:ks_sitemgr' . PHP_EOL .
                        '# createdate: ' . date('r') . PHP_EOL .
                        '# userfolder: ' . $GLOBALS['$TYPO3_CONF_VARS']['BE']['userHomePath'] . $userId . PHP_EOL .
                        '  usr_name                    = ' . $userId . PHP_EOL .
                        '  usr_root                    = ' . $pageId . PHP_EOL .
                        '  plugin.tx_sitemgr.username  = ' . $arg['title'] . PHP_EOL .
                        '  plugin.tx_sitemgr.useremail = ' . $arg['customerEmail'] . PHP_EOL .
                        '  plugin.tx_sitemgr.userId    = ' . $userId . PHP_EOL .
                        '  plugin.tx_sitemgr.rootPage  = ' . $pageId . PHP_EOL .
                        '######################################################################' . PHP_EOL,
                    'sitetitle' => $arg['customerName'],
                    'title' => 'template for ext:sitemgr, contains username const. only',
                    'root' => 1,
                ),
            ),
            //create customer
            'tx_sitemgr_customer' => array(
                'NEW61' => array(
                    'pid' => $pageId,
                    'title' => $arg['title'],
                    'main_be_user' => $userId,
                    'be_groups' => $groupId,
                ),
            ),
            //create acl
            'tx_beacl_acl' => array(
                'NEW51' => array(
                    'pid' => $pageId,
                    'type' => 0,
                    'object_id' => $userId,
                    'cruser_id' => $userId,   //set creator to owner
                    'permissions' => 27,       //do not delete rootpage, but allow all other things
                    'recursive' => 0,
                ),
                'NEW52' => array(
                    'pid' => $pageId,
                    'type' => 0,
                    'object_id' => $userId,   //allow all for subpages
                    'cruser_id' => $userId,   //set creator to owner
                    'permissions' => 31,
                    'recursive' => 1,
                ),
            ),
            //modify be user
            'be_users' => array(
                $userId => array(
                    'db_mountpoints' => $pageId,
                    'password' => $arg['password'],
                ),
            ),
        );

        //execute hook
        $this->executeDatabasePreprocessing(
            'customerCreateRound2',
            $data,
            array(
                'parentUid' => $arg['uid'],
                'description' => $arg['description'],
                'defaultLang' => $GLOBALS['BE_USER']->uc['lang'],
                'customerName' => $arg['title'],
                'customerParentGroup' => $tgroup['value'],
                'customerEmail' => $arg['customerEmail'],
                'customerPassword' => $arg['password'],
                'customerAdminUid' => $userId,
                'customerGroupUid' => $groupId,
                'customerRootPid' => $pageId,
            )
        );

        $tcemain = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
        $tcemain->start(
            $data,
            array(),
            $this->getBackendUserAsAdmin()
        );
        $tcemain->process_datamap();
        /***********************************************************************
         * Fix problem with updating password
         */
        $data['be_users'][$userId]['password'] = md5($arg['password']);
        $erg = $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
            'be_users',
            'uid=' . $userId,
            $data['be_users'][$userId]
        );
        /***********************************************************************
         * create user and group folder
         */

        $this->ensureUserHomeFolderExists($userId);
        $this->ensureGroupHomeFolderExists($groupId);

        /***********************************************************************
         * clear cache
         */
        if ($arg['copyPagesFrom'] == 'on') {
            $tcemain = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
            $tcemain->copyTree = 99;
            $tcemain->copyWhichTables = '*';
            $cmd = array(
                'pages' => array(
                    $arg['copyPagesFrom'] => array(
                        'copy' => $pageId
                    ),
                ),
            );
            $tcemain->start(
                array(),
                $cmd,
                $this->getBackendUserAsAdmin()
            );
            $tcemain->process_cmdmap();
        }

        $this->executeDatabasePreprocessing(
            'customerCreateRound3',
            $data,
            array(
                'parentUid' => $arg['uid'],
                'description' => $arg['description'],
                'defaultLang' => $GLOBALS['BE_USER']->uc['lang'],
                'customerName' => $arg['title'],
                'customerParentGroup' => $tgroup['value'],
                'customerEmail' => $arg['customerEmail'],
                'customerPassword' => $arg['password'],
                'customerAdminUid' => $userId,
                'customerGroupUid' => $groupId,
                'customerRootPid' => $pageId,
            )
        );

        /***********************************************************************
         * clear cache
         */
        $tcemain->clear_cacheCmd('pages');
    }

    /**
     * @param string $hookname
     * @param array $fields
     * @param array $params
     */
    private function executeDatabasePreprocessing($hookname, &$fields, $params)
    {
        /** @var \KayStrobach\Sitemgr\Utilities\FileSystemUtility $fileSystemUtility */
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['sitemgr'][$hookname])) {
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['sitemgr'][$hookname] as $userFunc) {
                \TYPO3\CMS\Core\Utility\GeneralUtility::callUserFunction($userFunc, $fields, $params, $this);
            }
        }
    }

    public function ensureUserHomeFolderExists($userId)
    {
        /** @var \KayStrobach\Sitemgr\Utilities\FileSystemUtility $fileSystemUtility */
        $fileSystemUtility = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\KayStrobach\Sitemgr\Utilities\FileSystemUtility::class);
        if (trim($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath']) !== '') {
            try {
                $fileSystemUtility->ensureFolderExists(rtrim($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath'], '/') . '/' . $userId);
            } catch (ExistingTargetFolderException $e) {
                // well this is fine
            }

        }
    }

    public function ensureGroupHomeFolderExists($groupId)
    {
        $fileSystemUtility = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\KayStrobach\Sitemgr\Utilities\FileSystemUtility::class);
        if (trim($GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath']) !== '') {
            try {
                $fileSystemUtility->ensureFolderExists(rtrim($GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath'], '/') . '/' . $groupId);
            } catch (ExistingTargetFolderException $e) {
                // well this is fine
            }
        }
    }

    /**
     * @param $uid
     * @return Customer
     */
    public function getCustomerForPage($uid)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var CustomerRepository $customerRepository */
        $customerRepository = $objectManager->get(CustomerRepository::class);
        return $customerRepository->findCustomerForPageRecursive($uid);
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUserAsAdmin()
    {
        /** @var BackendUserAuthentication $user */
        $user = clone $GLOBALS['BE_USER'];
        $user->user['admin'] = 1;
        return $user;
    }
}