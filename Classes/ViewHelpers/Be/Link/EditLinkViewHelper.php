<?php

namespace  KayStrobach\Sitemgr\ViewHelpers\Be\Link;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class EditLinkViewHelper extends AbstractTagBasedViewHelper
{
    protected $tagName = 'a';

    /**
     * @param string $parameters
     * @param string $returnUrl
     * @param string $class
     * @return string
     */
    public function render($parameters, $returnUrl = '', $class = '') {
        $uri = BackendUtility::getModuleUrl('record_edit') . '&' . $parameters;
        if (!empty($returnUrl)) {
            $uri .= '&returnUrl=' . rawurlencode($returnUrl);
        }
        $this->tag->addAttribute('class', $class);
        $this->tag->addAttribute('href', $uri);
        $this->tag->setContent($this->renderChildren());
        $this->tag->forceClosingTag(TRUE);
        return $this->tag->render();
    }
}