.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _sponsors:

Sponsors & supporters
---------------------

:version 3.0:
        The mayor refactoring and rewrite for TYPO3 8 LTS has been sponsored and influenced by WNG

:version 1.x and 2.x:
        First development was sponsored by the Saxonian Educational Server www.sachsen.schule

