<?php

namespace KayStrobach\Sitemgr\Hooks;


use KayStrobach\Sitemgr\Domain\Model\Customer;
use KayStrobach\Sitemgr\Domain\Repository\CustomerRepository;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DataHandlerCmdHook
{
    /**
     * @param DataHandler $dataHandler
     */
    public function processCmdmap_afterFinish(DataHandler $dataHandler)
    {
        if (is_array($dataHandler->cmdmap['pages'])) {
            foreach ($dataHandler->cmdmap['pages'] as $uid => $cmdArray) {
                if (!isset($cmdArray['copy'])) {
                    continue;
                }
                $pageRow = BackendUtility::getRecord('pages', $uid, 'doktype,title,pid');
                if (($pageRow !== null) && ($pageRow['doktype'] === CustomerRepository::CUSTOMER_DOCTYPE)) {
                    $this->cleanupCustomer($pageRow, $uid);
                    $this->prepareCustomer($pageRow, $uid, $dataHandler);
                }
            }
        }
    }

    public function cleanupCustomer($pageRow, $uid)
    {

    }

    public function prepareCustomer($pageRow, $uid, DataHandler $dataHandler)
    {
        /** @var DataHandlerDataHook $dataHandlerHook */
        $dataHandlerHook = GeneralUtility::makeInstance(DataHandlerDataHook::class);
        $dataHandlerHook->handleNewEntry(
            'pages',
            $uid,
            $pageRow,
            $dataHandler
        );
    }
}