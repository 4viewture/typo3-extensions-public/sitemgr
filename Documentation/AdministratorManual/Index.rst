﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _admin-manual:

Administrator Manual
====================

Target group: **Administrators**


Installation
------------

Install the extension via Extensionmanager, check if be_acl is also installed.

Use the gear icon of the extension sitemgr to change one of the following options

.. figure:: ../Images/AdministratorManual/2017-05-04_11-59-06.png
	:width: 700px
	:alt: Extension Manager setup

	Extension Manager setup


Setup in the installtool
------------------------

Please set the following options in the installtool

.. code-block:: php

    $GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath']
    $GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath']

Both folders are used to map the uid of either the group or the user to a corresponding path.

.. figure:: ../Images/AdministratorManual/2017-05-04_20-14-40.png
	:width: 400px
	:alt: automatically created group folders

    	automatically created group folders

The sitemgr will ensure, that the group and user folders are created during customer and user creation.

You can check if these settings are correct, if you open the check module delivered with sitemgr.

Setup in the pagetree
---------------------

You should set 2 options in the Pagets.

.. code-block:: php

    mod.web_txsitemgr {
      customer {
        # group with default permissions (modules / plugins)
        createUser.group = 3
        # page where customer creation is allowed
        customerPidPage = 1
      }
    }


The createUser.group is used to automatically assign rights to the users based on a template group.

The customerPidPage is used to ensure, that customers are only created as direct childs of the given page.

Check your settings
-------------------

The sitemgr ships a module to check your settings, please consult it to ensure everything is setup properly.

To do so, please open the customer module and then select a customer / page in the pagetree.

Then switch to the submodule called *systemstatus*.

.. figure:: ../Images/AdministratorManual/2017-05-04_12-05-10.png
	:width: 700px
	:alt: system check in customer module

    	system check in customer module

If the check found problems, it tells you so and tries to give you a hint how to solve it.

.. figure:: ../Images/AdministratorManual/2017-05-04_12-06-54.png
	:width: 700px
	:alt: system check in customer module with a failed check


Edgecase non administrators should manage customers
---------------------------------------------------

General Stuff:

* Go to the Extensionmanager an run the update script

Access to pagetree:

* Open Access Module
* Add Permissions to the above group (in tree)
* Open Backend Users Module in the group section edit Group "Sitemgr Manager"
* Add pagetree mount to user

Access to Folders:

* Open Backend Users Module in the group section edit Group "Sitemgr Manager"
* Add filemount which allows to access the userhome folder and group home folder