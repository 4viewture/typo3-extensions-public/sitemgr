<?php

namespace KayStrobach\Sitemgr\Domain\Service;


use KayStrobach\Sitemgr\Domain\Model\Customer;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class AclService
{
    public function getPagePermissions($backendUser)
    {
        $user = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord(
            'be_users',
            (int)$backendUser
        );

        $grants   = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecordsByField(
            'tx_beacl_acl',
            'object_id',
            (int)$backendUser,
            'AND type="0"'
        );

        $return = array();
        foreach($grants as $grant) {
            $path     = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecordPath(
                $grant['pid'],
                '',
                100
            );
            $return[$grant['pid']] = array(
                'userName' => $user['username'],
                'path'     => $path,
                'uid'      => $grant['uid'],
                'right'    => 'R/W',
                'pid'      => $grant['pid'],
            );
        }
        return $return;
    }

    public function addPagePermission($pid, $backendUser)
    {
        $customer = $this->getCustomerService()->getCustomerForPage($pid);

        if (!$customer instanceof Customer) {
            return false;
        }

        if(!$customer->isAllowedToManageUser($backendUser)) {
            return false;
        }
        //add mountpoint
        $user = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord(
            'be_users',
            $backendUser
        );
        $user['db_mountpoints'] = \TYPO3\CMS\Core\Utility\GeneralUtility::uniqueList(
            $user['db_mountpoints'] . ',' . (int)$pid
        );
        $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
            'be_users',
            'uid=' . (int)$backendUser,
            $user
        );
        //add grants
        $GLOBALS['TYPO3_DB']->exec_INSERTquery(
            'tx_beacl_acl',
            array(
                'pid'         => (int)$pid,
                'cruser_id'   => $GLOBALS['BE_USER']->user['uid'],
                'type'        => 0, //user
                'object_id'   => $backendUser,
                'permissions' => 27,
                'recursive'   => 0
            )
        );
        $GLOBALS['TYPO3_DB']->exec_INSERTquery(
            'tx_beacl_acl',
            array(
                'pid'         => $pid,
                'cruser_id'   => $GLOBALS['BE_USER']->user['uid'],
                'type'        => 0, //user
                'object_id'   => $backendUser,
                'permissions' => 31,
                'recursive'   => 1
            )
        );
        return true;
    }

    public function removePagePermission($pid, $backendUser)
    {
        $customer = $this->getCustomerService()->getCustomerForPage($pid);

        if (!$customer instanceof Customer) {
            return false;
        }

        if(!$customer->isAllowedToManageUser($backendUser)) {
            return false;
        }

        //drop acls
        $GLOBALS['TYPO3_DB']->exec_DELETEquery(
            'tx_beacl_acl',
            'pid=' . (int)$pid . ' AND object_id=' . (int)$backendUser . ' AND type=0'
        );
        //drop mountpoints
        $user = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord(
            'be_users',
            (int)$backendUser
        );
        $user['db_mountpoints'] = \TYPO3\CMS\Core\Utility\GeneralUtility::rmFromList(
            $pid,
            $user['db_mountpoints']
        );
        $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
            'be_users',
            'uid=' . (int)$backendUser,
            $user
        );
    }

    /**
     * @return CustomerService
     */
    public function getCustomerService()
    {
        return GeneralUtility::makeInstance(CustomerService::class);
    }
}