<?php

namespace KayStrobach\Sitemgr\Domain\Model;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Domain extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var string
     */
    protected $domainName;

    /**
     * @var string
     */
    protected $redirectTo;

    /**
     * @var int
     */
    protected $redirectHttpStatusCode;

    /**
     * @var boolean
     */
    protected $prependParams;

    /**
     * @var boolean
     */
    protected $forced;

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domainName;
    }

    /**
     * @param string $domainName
     */
    public function setDomainName($domainName)
    {
        $this->domainName = $domainName;
    }

    /**
     * @return string
     */
    public function getRedirectTo()
    {
        return $this->redirectTo;
    }

    /**
     * @param string $redirectTo
     */
    public function setRedirectTo($redirectTo)
    {
        $this->redirectTo = $redirectTo;
    }

    /**
     * @return int
     */
    public function getRedirectHttpStatusCode()
    {
        return $this->redirectHttpStatusCode;
    }

    /**
     * @param int $redirectHttpStatusCode
     */
    public function setRedirectHttpStatusCode($redirectHttpStatusCode)
    {
        $this->redirectHttpStatusCode = $redirectHttpStatusCode;
    }

    /**
     * @return string
     */
    public function getPrependParams()
    {
        return $this->prependParams;
    }

    /**
     * @param string $prependParams
     */
    public function setPrependParams(string $prependParams)
    {
        $this->prependParams = $prependParams;
    }

    /**
     * @return bool
     */
    public function isForced()
    {
        return $this->forced;
    }

    /**
     * @param bool $forced
     */
    public function setForced($forced)
    {
        $this->forced = $forced;
    }
}