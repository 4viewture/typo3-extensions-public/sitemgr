<?php

namespace KayStrobach\Sitemgr\Domain\Model;


use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Customer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\BackendUser
     */
    protected $mainBeUser = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\BackendUser>
     */
    protected $adminBeUsers;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\BackendUser>
     */
    protected $normalBeUsers;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Beuser\Domain\Model\BackendUserGroup>
     */
    protected $beGroups;

    public function __construct()
    {
        $this->adminBeUsers = new ObjectStorage();
        $this->normalBeUsers = new ObjectStorage();
        $this->beGroups = new ObjectStorage();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\BackendUser
     */
    public function getMainBeUser()
    {
        return $this->mainBeUser;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\BackendUser $mainBeUser
     */
    public function setMainBeUser(\TYPO3\CMS\Extbase\Domain\Model\BackendUser $mainBeUser)
    {
        $this->mainBeUser = $mainBeUser;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\BackendUser>
     */
    public function getAdminBeUsers()
    {
        return $this->adminBeUsers;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\BackendUser> $adminBeUsers
     */
    public function setAdminBeUsers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $adminBeUsers)
    {
        $this->adminBeUsers = $adminBeUsers;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\BackendUser>
     */
    public function getNormalBeUsers()
    {
        return $this->normalBeUsers;
    }

    public function getNormalBeUsersCommaSeparated() {
        if ($this->getNormalBeUsers() instanceof \Iterator) {
            $users = [];
            /** @var \TYPO3\CMS\Extbase\Domain\Model\BackendUser $user */
            foreach ($this->getNormalBeUsers() as $user) {
                $users[$user->getUid()] = $user;
            }
            return implode(',', array_keys($users));
        }
        return '';
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\BackendUser> $normalBeUsers
     */
    public function setNormalBeUsers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $normalBeUsers)
    {
        $this->normalBeUsers = $normalBeUsers;
    }

    public function getAllBeUsers() {
        $users = [];
        if ($this->getMainBeUser() !== null) {
            $users[$this->getMainBeUser()->getUid()] = $this->getMainBeUser();
        }
        if ($this->getAdminBeUsers() instanceof \Iterator) {
            /** @var \TYPO3\CMS\Extbase\Domain\Model\BackendUser $user */
            foreach($this->getAdminBeUsers() as $user) {
                $users[$user->getUid()] = $user;
            }
        }
        if ($this->getNormalBeUsers() instanceof \Iterator) {
            /** @var \TYPO3\CMS\Extbase\Domain\Model\BackendUser $user */
            foreach ($this->getNormalBeUsers() as $user) {
                $users[$user->getUid()] = $user;
            }
        }
        return $users;
    }

    /**
     * @param $uid
     * @return bool
     */
    public function isAllowedToManageUser($uid)
    {
        $users = $this->getAllBeUsers();
        if (array_key_exists($uid, $users)) {
            return true;
        }
        return false;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getBeGroups()
    {
        return $this->beGroups;
    }

    public function getBeGroupsCommaSeparated()
    {
        $groups = [];
        /** @var \TYPO3\CMS\Beuser\Domain\Model\BackendUserGroup $group */
        foreach ($this->beGroups as $group) {
            $groups[] = $group->getUid();
        }
        return implode(',', $groups);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $beGroups
     */
    public function setBeGroups(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $beGroups)
    {
        $this->beGroups = $beGroups;
    }

    public function getRow()
    {
        return BackendUtility::getRecord(
            'pages',
            $this->getUid()
        );
    }
}