<?php

namespace KayStrobach\Sitemgr\Domain\Repository;


use KayStrobach\Sitemgr\Domain\Model\Customer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

class CustomerRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    const CUSTOMER_DOCTYPE = 157;

    /**
     * Returns all objects of this repository.
     *
     * @return QueryResultInterface|array
     * @api
     */
    public function findAllinWholeSystem()
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('doktype', self::CUSTOMER_DOCTYPE)
        );
        $query->getQuerySettings()->setIgnoreEnableFields(['hidden']);
        $query->getQuerySettings()->setRespectStoragePage(false);
        return $query->execute();
    }

    /**
     * @param $pageId
     * @return Customer
     * @throws \Exception
     * @internal param $pid
     */
    public function findCustomerForPageRecursive($pageId)
    {
        $sys_page = GeneralUtility::makeInstance(PageRepository::class);
        $rootLine = $sys_page->getRootLine($pageId);
        $i = count($rootLine) - 1;
        $customer = null;
        do {
            $uid = $rootLine[$i]['uid'];
            $customer = $this->findCustomerForPage($uid);
            $i--;
        } while ($customer == null && isset($rootLine[$i]));
        return $customer;
    }

    /**
     * @param $pageId
     * @return Customer
     */
    public function findCustomerForPage($pageId)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching(
            $query->logicalAnd(
                [
                    $query->equals('uid', $pageId),
                    $query->equals('doktype', self::CUSTOMER_DOCTYPE)
                ]
            )

        );
        $query->getQuerySettings()->setIgnoreEnableFields(['hidden']);
        $query->getQuerySettings()->setRespectStoragePage(false);
        return $query->execute()->getFirst();
    }
}