<?php

namespace KayStrobach\Sitemgr\Controller\Backend;


class SystemController extends AbstractBackendController
{
    /**
     * @inject
     * @var \KayStrobach\Sitemgr\Utilities\SystemCheckUtility
     */
    protected $systemCheckUtility;

    /**
     * Displays all Rooms
     *
     * @return string The rendered list view
     */
    public function indexAction()
    {
        $this->view->assign('checks', $this->systemCheckUtility->getCheckResults());
        $this->view->assign('currentUser', $this->getBackendUserAuthentication());
    }
}