<?php

namespace KayStrobach\Sitemgr\Domain\Repository;


use KayStrobach\Sitemgr\Domain\Model\Customer;

class DomainRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function findByCustomer(Customer $customer)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('pid', $customer->getUid())
        );
        $query->getQuerySettings()->setRespectStoragePage(false);
        return $query->execute();
    }
}