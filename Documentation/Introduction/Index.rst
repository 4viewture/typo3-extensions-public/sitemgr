﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


What does it do?
================

EXT:sitemgr gives you the possibility to let customers manage their own users, without being admin.

This is possible, because users can get the right to manage an other group, which means, they can manage all members of the group.

.. figure:: ../Images/Introduction/sitemgr-customer.png
	:width: 700px
	:alt: Customer management module

	Customer management module, when a customer is selected

Current Features
----------------

Features for TYPO3 Admins (flagged with admin in the user record)

* can create new customers with drag and drop in the page tree.
* can manage customer backend_users filtered in a special module
* can set user rights via a simplified module for extension be_acl to make adding rights fast
* can change customer name

Customer administrator

* can manage customer backend_users filtered in a special module
* can set user rights via a simplified module for extension be_acl to make adding rights fast

Planned Features
----------------

* Additional permissions to allow non TYPO3 admins to manage users

Requirements
------------

* TYPO3 CMS
* be_acl

See composer.json for version details.