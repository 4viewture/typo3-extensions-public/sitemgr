.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _data-structures-manual:

Data structures
===============

Target group: **Administrators** and **Developers**

special pagetype
----------------

We use a special pagetype to indicate that a page is a customer.
This pagetype has a limited set of fields, which are only meant for managing customers.

The doktype of this pagetype is defined in the *CustomerRepository*.


.. code-block:: php

    const CUSTOMER_DOCTYPE = 157;


Additionally these pages contain the following fields:

* customermainuser
* customeradminusers
* normalusers
* backendusergroup

customermainuser
----------------

This user is automatically created during the creation of a customer.
This user is the main administrator of a customer and has some special rights and all the rights of the customeradminusers.

customeradminusers
------------------

These users are able to manage the users and the domain records of the customer.
Additionally other special rights may be assigned to the users mapped in this field.

normalusers
-----------

These users are the users manageable by the former defined admin users.
They have no special rights and may only access parts of the tree if admin users have assigned permissions to them explicitly.

You can grant pagetree permissions to every of this users if you are allowed to do so.

backendusergroup
----------------

This is the main group assigned to all users in this customer.
Having this group the user can access the automatically created *groupHomePath*.