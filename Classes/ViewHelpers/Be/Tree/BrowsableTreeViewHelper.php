<?php

namespace  KayStrobach\Sitemgr\ViewHelpers\Be\Tree;

use KayStrobach\Sitemgr\Backend\Tree\View\BrowseTreeView;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Security\Cryptography\HashService;
use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormFieldViewHelper;

class BrowsableTreeViewHelper extends AbstractFormFieldViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'input';

    /**
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * @param int $pid
     * @param int $width
     * @param int $height
     * @return string
     */
    public function render($pid = 0, $width = 250, $height = 350)
    {
        $name = $this->getName();
        $this->registerFieldNameForFormTokenGeneration($name);
        $this->setRespectSubmittedDataValue(true);

        $this->tag->addAttribute('type', 'hidden');
        $this->tag->addAttribute('name', $name);
        $this->tag->addAttribute('value', $this->getValueAttribute());

        $this->registerFieldNameForFormTokenGeneration($name);
        $this->setRespectSubmittedDataValue(true);

        $this->addAdditionalIdentityPropertiesIfNeeded();
        $this->setErrorClassAttribute();

        /** @var BrowseTreeView $treeView */
        $treeView = GeneralUtility::makeInstance(BrowseTreeView::class);
        $treeView->init();
        $treeView->ext_IconMode = true;

        $treeUuid = md5($name);
        $this->tag->addAttribute('data-hiddenTreeField', $treeUuid);
        $treeView->setDataIdentifier($treeUuid);

        $treeView->thisScript = $this->getUri($pid);
        $treeView->MOUNTS = [$pid];
        $output = $treeView->getBrowsableTree();
        return  $this->tag->render() . '<div style="width:' . (int)$width . 'px;height:' . (int)$height . 'px;margin-left: 25px;" class="treeWrapper">' . $output . '</div>';
    }

    protected function getUri($pageUid)
    {
        $uriBuilder = $this->controllerContext->getUriBuilder();


        return $uriBuilder
            ->reset()
            ->setTargetPageUid($pageUid)
            ->setArguments($uriBuilder->getRequest()->getArguments())
            ->uriFor(
                $this->controllerContext->getRequest()->getControllerActionName(),
                $this->controllerContext->getRequest()->getArguments(),
                $this->controllerContext->getRequest()->getControllerName(),
                $this->controllerContext->getRequest()->getControllerExtensionName(),
                $this->controllerContext->getRequest()->getPluginName()
            );
    }
}