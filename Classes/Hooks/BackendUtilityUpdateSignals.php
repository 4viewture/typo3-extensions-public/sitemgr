<?php

namespace KayStrobach\Sitemgr\Hooks;


use TYPO3\CMS\Backend\Utility\BackendUtility;

class BackendUtilityUpdateSignals
{
    public function updateModule(&$params, $ref)
    {
        $moduleUrl = BackendUtility::getModuleUrl('web_SitemgrTxSitemgrMod1', ['id' => $params['parameter']['id']]);


        $params['JScode'] = '
            if (typeof top.TYPO3.Backend !== undefined) {
                top.jump("' . $moduleUrl . '", "web_SitemgrTxSitemgrMod1", "web", ' . $params['parameter']['id'] . ')
            }
        ';
    }

    public function startCustomerEdit(&$params, $ref)
    {
        $id = (int)$params['parameter']['id'];
        $uriParameters = [
            'edit' => [
                'pages' => [
                    $id => 'edit'
                ]
            ],
            'id' => $id,
            'returnUrl' => BackendUtility::getModuleUrl('web_SitemgrTxSitemgrMod1', ['id' => $id])
        ];

        $moduleUrl = BackendUtility::getModuleUrl('record_edit', $uriParameters);
        $params['JScode'] = '
            if (typeof top.TYPO3.Backend !== undefined) {
                top.jump("' . $moduleUrl . '", "web_SitemgrTxSitemgrMod1", "web", ' . $id . ')
            }
        ';
    }
}