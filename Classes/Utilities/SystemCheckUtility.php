<?php

namespace KayStrobach\Sitemgr\Utilities;


use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class SystemCheckUtility implements SingletonInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
    }

    public function getCheckResults()
    {
        $extensionConfiguration = $this->getExtensionConfiguration();
        $checks = [
            'rootCustomerPageEnforced' => (bool)$extensionConfiguration['customerPidPageTS']['value'],
            'rootCustomerPageDefinedInPageTS' => false,
            'enforceUserNamesWithPrefix' => (bool)$extensionConfiguration['forceBeUserPrefix']['value'],
            'shouldCreateUserHomePath' => (bool)strlen($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath']),
            'userHomePath' => (string)$GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath'],
            'userHomePathExists' => $this->checkIfFolderExists($GLOBALS['TYPO3_CONF_VARS']['BE']['userHomePath']),
            'shouldCreateGroupHomePath' => (bool)strlen($GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath']),
            'groupHomePath' => (string)$GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath'],
            'groupHomePathExists' => $this->checkIfFolderExists($GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath']),
            'newUserDefault' => [
                'group' => $this->getPageTsSettings('customer.createUser.group'),
            ],
            'customerPidPage' => $this->getPageTsSettings('customer.customerPidPage'),
        ];
        return $checks;
    }

    public function hasErrors()
    {
        $checks = $this->getCheckResults();

        if (!$checks['shouldCreateUserHomePath']) {
            return true;
        }
        if (!$checks['userHomePathExists']) {
            return true;
        }
        if (!$checks['shouldCreateGroupHomePath']) {
            return true;
        }
        if (!$checks['groupHomePathExists']) {
            return true;
        }
        if (!$checks['newUserDefault']['group']) {
            return true;
        }
        return false;
    }

    protected function getExtensionConfiguration()
    {
        $configurationUtility = $this->objectManager->get('TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility');
        return $configurationUtility->getCurrentConfiguration('sitemgr');
    }

    protected function getPageTsSettings($path = '')
    {
        if (isset($_GET['id'])) {
            $id = intval($_GET['id']);
        } else {
            $id = 0;
        }
        if ($path !== '') {
            $path = '.' . $path;
        }

        $result = $this->getBackendUserAuthentication()->getTSConfig(
            'mod.web_txsitemgr' . $path,
            BackendUtility::getPagesTSconfig($id)
        );

        if ($path !== '') {
            if (array_key_exists('value', $result)) {
                return $result['value'];
            }
        }
        return $result;
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

    protected function checkIfFolderExists($combinedIdentifier)
    {
        try {
            $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
            $folder = $resourceFactory->getFolderObjectFromCombinedIdentifier($combinedIdentifier);
        } catch (InsufficientFolderAccessPermissionsException $exception) {
            return true;
        } catch (\Exception $exception) {
            return false;
        }
        if ($folder instanceof Folder) {
            return true;
        }
        return false;
    }
}