<?php


$GLOBALS['TCA']['be_groups']['columns'] = array_merge_recursive(
    $GLOBALS['TCA']['be_groups']['columns'],
    [
        'sitemgr_uuid' => [
            'exclude' => 0,
            'label' => '',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
    ]
);